﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    class Brain
    {
        private ConcurrentDictionary<Point, ConcurrentStack<Neuron>> _neurons = new ConcurrentDictionary<Point, ConcurrentStack<Neuron>>();
        ProjectileManager _projectiles;
        SensorManager _sensors;
        ActuatorManager _actuators;
        SynapseManager _synapses;
        List<Neuron> LastTickNeuronsToUpdate { get; set; } = new List<Neuron>();
        public int Age { get; private set; }
        #region "Generation constants"
        public const int EARLY_CHILDHOOD_SYNAPTIC_PRUNING_TICKCOUNT = 1000;
        #endregion

        public RenderInfo Render { get; private set; }
        
        public Brain(ConcurrentDictionary<Point, ConcurrentStack<Neuron>> population, SynapseManager synapses, SensorManager sensors, ActuatorManager actuators, ProjectileManager projectiles, RenderInfo render)
        {
            Render = render;
            _neurons = population;
            _sensors = sensors;
            _actuators = actuators;
            _projectiles = projectiles;
            _synapses = synapses;
            SetupActuators();
        }

        private void SetupActuators()
        {
            //ensures neurons get set to an actuator
            foreach(var act in _actuators.Actuators)
            {
                foreach(var n in act.GetNeuronsToActivate())
                {
                    n.Actuator = act;
                }
            }
        }


        public void DrawBrain(Graphics g)
        {
            for(int i=0; i < Render.GridSize.Width; i++)
            {
                var xCoord = (int)(Render.SizePerGridSquare.Width * i) + (int)Render.RenderOffset.X;
                var topPoint = new Point(xCoord, 0);
                var bottomPoint = new Point(xCoord, (int)(Render.SizePerGridSquare.Height * Render.GridSize.Height));
                g.DrawLine(Pens.Black, topPoint, bottomPoint);
            }
            for(int i=0; i < Render.GridSize.Height; i++)
            {
                var yCoord = (int)(Render.SizePerGridSquare.Height * i) + (int)Render.RenderOffset.Y;
                var rightPoint = new Point(0, yCoord);
                var leftPoint = new Point((int)(Render.SizePerGridSquare.Width * Render.GridSize.Width), yCoord);
                g.DrawLine(Pens.Black, rightPoint, leftPoint);
            }
            foreach (var cellGroup in _neurons.Keys)
            {
                foreach(var n in _neurons[cellGroup])
                {
                    n.Draw(g, Render);
                }
            }
            _synapses.Draw(g, Render);
            _projectiles.DrawProjectiles(g, Render);
        }
        public void Update()
        {
            //first, check sensors
            LastTickNeuronsToUpdate.Clear();
            var newSourceSignals = _sensors.Sensors.SelectMany(s => s.Update(_projectiles)).ToList();
            _projectiles.UpdateProjectiles(Render, _synapses.Synapses);
            foreach (var cellGroup in _neurons.Keys)
            {
                foreach (var n in _neurons[cellGroup])
                {
                    n.Update(_projectiles);
                }
            }
            Age++;
            if (Age % 100 == 0 && Age != EARLY_CHILDHOOD_SYNAPTIC_PRUNING_TICKCOUNT)
            {
                foreach (var cellGroup in _synapses.Synapses.Keys)
                {
                    foreach(var s in _synapses.Synapses[cellGroup])
                    {
                        s.Decay();
                    }
                }
            }
            if(Age == EARLY_CHILDHOOD_SYNAPTIC_PRUNING_TICKCOUNT)
            {
                //initiate pruning
            }
        }

}
}

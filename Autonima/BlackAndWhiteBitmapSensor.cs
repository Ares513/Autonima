﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    /// <summary>
    /// This is a first pass at computer vision for Autonima. 
    /// It stores a black and white Bitmap object.
    /// At a given interval ticksBetweenPercepts, it will fire the nodes in the list that correspond to active pixels.
    /// That is to say, that if a pixel at (10, 10) has color, it will triger the axons of the neuron mapped to 10,10.
    /// 
    /// For now, PerceptSize and TicksBetweenPercepts do nothing; in the future, they will describe a sort of "focus" for eyesight.
    /// My intuition says that humans focus on things so there isn't as much information to process all at once. 
    /// </summary>
    public class BlackAndWhiteBitmapSensor : ISensorArray
    {
        Dictionary<Point, Neuron> _nodesToFire;
        public Vector2 Percept
        {
            get
            {
                return new Vector2(_truePercept.X + PerceptSize / 2, _truePercept.Y + PerceptSize / 2);
            }
        }
        private Vector2 _truePercept;
        private int PerceptSize { get; set; }
        private int TicksBetweenPercepts { get; set; }
        private int _ticksSoFar = 0;
        private Bitmap _input;
        public BlackAndWhiteBitmapSensor(Bitmap input, IEnumerable<Neuron> nodesToFire, int perceptSize, int ticksBetweenPercepts)
        {
            _nodesToFire = new Dictionary<Point, Neuron>();
            _input = input;
            _truePercept = Vector2.Zero;
            var enumerator = nodesToFire.GetEnumerator();
            enumerator.MoveNext();
            for(int i=0; i < PerceptSize; i++)
            {
                for(int j=0; j < PerceptSize; j++)
                {
                    _nodesToFire[new Point(i, j)] = enumerator.Current;
                    enumerator.Current.MakeSourceSignal();
                    enumerator.MoveNext();

                }
            }
            PerceptSize = perceptSize;
            TicksBetweenPercepts = ticksBetweenPercepts;
        }
        public List<Neuron> Update(ProjectileManager manager)
        {
            var output = new List<Neuron>();
            _ticksSoFar++;
            if(TicksBetweenPercepts == _ticksSoFar)
            {
                _ticksSoFar = 0;
                //ToDo: optimize to only check the percept
                for (int i=0; i < PerceptSize; i++)
                {
                    for(int j=0; j < PerceptSize; j++)
                    {
                        var target = new Point(i, j);
                        if(IsPointInPercept(target))
                        {
                            
                            var pixelAtPoint = _input.GetPixel((int)(_truePercept.X) + i, (int)(_truePercept.Y) + j);
                            if (!pixelAtPoint.Equals(Color.FromArgb(255, 255, 255, 255)))
                            {
                                var thisNeuron = _nodesToFire[target];
                                thisNeuron.Fire(manager);
                                output.Add(thisNeuron);
                            }
                        }

                    }
                }
            }
            return output;
        }

        private bool IsPointInPercept(Point target)
        {
            return target.Distance(Percept.ToPointTruncate()) < PerceptSize;
        }

        public void ShiftPercept(Vector2 velocity)
        {
            _truePercept += velocity;
            var clampedPercept = Vector2.Clamp(_truePercept, Vector2.Zero, new Vector2(_input.Size.Width - PerceptSize, _input.Size.Height - PerceptSize));
            _truePercept = clampedPercept;
            
        }
    }
}

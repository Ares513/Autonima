﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    /// <summary>
    /// Manages a list of sensors which are checked every BrainTick.
    /// </summary>
    public class SensorManager
    {
        public List<ISensorArray> Sensors { get; private set; } = new List<ISensorArray>();
        public void AddSensor(ISensorArray sensor) => Sensors.Add(sensor);

    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class NeurogenesisNeuronFactory : INeuronFactory
    {
        private NeuronFactoryUIController _uiController;

        public NeurogenesisNeuronFactory(NeuronFactoryUIController uiController)
        {
            _uiController = uiController;
        }

        public Tuple<ConcurrentDictionary<Point, ConcurrentStack<Neuron>>, ConcurrentDictionary<Point, ConcurrentStack<Synapse>>> GenerateNeurons(int count)
        {
            throw new NotImplementedException();
        }

        public void Draw(RenderInfo render, Graphics g)
        {

        }

        public NeuronFactoryUIController GetUIController()
        {
            throw new NotImplementedException();
        }
    }
}

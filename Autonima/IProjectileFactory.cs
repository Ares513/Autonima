﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Numerics;

namespace Autonima
{
    public interface IProjectileFactory
    {
        Projectile MakeProjectile(Vector2 sourceLoc, Vector2 targetLoc, float speed, bool isSourceSignal);
    }
}

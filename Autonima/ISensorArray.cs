﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public interface ISensorArray
    {
        List<Neuron> Update(ProjectileManager manager);
    }
}
